---
presentation:
    theme: white.css
    minScale: 0.1
    maxScale: 1.5
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom 
    transitionSpeed: default
---

<!-- slide -->
# 第10章：字符串 :book: P253

<!-- slide -->
# 本章学习内容

1. 字符串常量（字符串字面量）
1. 字符串处理函数
1. 字符数组和字符指针
1. 向函数传递字符串
1. 从函数返回一个字符串指针

<!-- slide -->
# 10.1 字符串常量

1. 由一对 **双引号括起来** 的一个字符序列
1. 为确定字符串的边界，C编译器自动在字符串末尾添加 **`'\0'`** 结束符

:question: 字符串常量 ==> 字符串字面量

<!-- slide -->
# 10.2 字符串的存储 :book: P253

## C没有字符串类型，使用字符数组和字符指针来处理

## 字符数组指每个元素都是 **`char`**

```dot
digraph c_string{
    bgcolor=transparent;
    rankdir = LR;
    
    node[shape=box, style=filled, fillcolor=antiquewhite];

    char_array_note[label="字符数组\n但不是字符串"] ;
    c_string_note[label="字符串"];

    node[shape=record,style=filled,fillcolor=white];

    char_array_graph[
         label="H|e|l|l|o"
        ]
    
    c_string_graph[
         label="H|e|l|l|o|\\0"
        ]

    char_array_note -> char_array_graph;
    c_string_note -> c_string_graph;
}
```

<!-- slide -->
# 字符串的初始化 :book: P254

```c{.line-numbers}
char str[6] = {'h','e','l','l','o','\0'};
char str[] = {'h','e','l','l','o','\0'};
char str[] = {"China"};
char str[] = "China";
```

<!-- slide -->
# 10.3 字符串指针 :book: P255

## 字符串指针（string pointer）： 指向字符串数据的指针变量

```c{.line-numbers}
char *ptr = "hello1";
*ptr = 'w';//undefined behavior

char str[10] = "hello2";
char *ptr = str;

str = "sorry";//error, can't assign to a array name
```

## 数组名存放在符号表

## 数组名是数组首元素的地址 :book: P255

```dot
digraph array_string{
    bgcolor="transparent";
    
    node[shape=record]
    array_string[
        label="h|e|l|l|o|2|\\0|||"
    ]
}
```

<!-- slide -->
# 字符串的访问和输入/输出 :book: P256

## 方式01： `%c`逐字符输入/输出

```c{.line-numbers}
for(i = 0; i < 10; ++i){
    scanf("%c", &str[i]);
    printf("%c", str[i]);
}
```

```c{.line-numbers}
for(i = 0; str[i] != '\0'; ++i){
    printf("%c", str[i]);
}
```

<!-- slide -->
# 字符串的访问和输入/输出 :book: P257

## 方式02： `%s`整体输入/输出

```c{.line-numbers}
scanf("%s", str);
printf("%s", str);
```

>:warning: :book: P258 用 **`%d`** 输入数字或 **`%s`** 输入字符串时，忽略 **空格**、**回车** 或 **制表符** 等 **空白字符** （被作为数据的分隔符），读到这些字符时，**程序** 认为数据读入结束，因此，**`scanf`** 按 **`%s`** 时不能输入带空格的字符串

<!-- slide -->
# 字符串的访问和输入/输出 :book: P258

## 方式03： `gets()/puts()`整体输入/输出

1. **`gets()`** 以 **回车符** 作为字符串输入的终止符，同时将回车符从输入缓冲区读走，但回车符不作为字符串的一部分
1. **`scanf()`** 不读走回车符，回车符仍保留在输入缓冲区中
1. **`puts()`** 输出时自动输出一个回车符

<!-- slide -->
# 限制输入字符串长度 :book: P259

1. **`gets()`** 和 **`scanf()`** 不能限制输入字符串的长度，容易引起缓冲区溢出（应该是堆栈溢出？）
1. **`char * fgets(char *str, int count, FILE *stream)`**
1. **`int fputs( const char *str, FILE *stream )`**
1. **`stdin`** 代表标准输入流，**`stdout`** 代表标准输出流， **`stderr`** 代表标准错误流

<!-- slide -->
# :book: P260

```c{.line-numbers}
//string literal copy to array
char str[] = "\"Hello\", I said to";

//pointer point to string literal
char *ptr = "\"Hello\", I said to";
```

<!-- slide -->
# 10.5 字符串处理函数:book: P261

```c{.line-numbers}
#include <string.h>

size_t strlen( const char *str );

char *strcpy( char *dest, const char *src );
char *strncpy( char *dest, const char *src, size_t count );

int strcmp( const char *lhs, const char *rhs );
int strncmp( const char *lhs, const char *rhs, size_t count );

char *strcat( char *dest, const char *src );
char *strncat( char *dest, const char *src, size_t count );
```

<!-- slide -->
# `size_t`

```c{.line-numbers}
typedef /*implementation-defined*/ size_t;

size_t is the unsigned integer type of the result of sizeof ,\
alignof (since C11) and offsetof, depending on the data model.

size_t can store the maximum size of a theoretically \
possible object of any type (including array).

size_t is commonly used for array indexing and loop counting.\
Programs that use other types, such as unsigned int,\
for array indexing may fail on,\
e.g. 64-bit systems when the index exceeds UINT_MAX or\
if it relies on 32-bit modular arithmetic.
```

<!-- slide -->
# `size_t`（续）

```c{.line-numbers}
printf("size==>%lu,max==>%lu",sizeof(size_t),SIZE_MAX);

//output in Xcode will be
//size==>8,max==>18446744073709551615
```
<!-- slide -->
# :book: P262 例10.4

## :book: P264 字符串赋值

## :book: P264 字符串比较与字符编码

<!-- slide -->
# `strlen`

```c{.line-numbers}
size_t strlen( const char *str );

char str[10] = {"China"};
printf("%d", strlen(str));//5

len = strlen(str);
for (i=0; i<len; i++){
	putchar(str[i]);
}
putchar('\n');
```

<!-- slide -->
# `strcpy`

```c{.line-numbers}
char *strcpy( char *dest, const char *src );
char *strncpy( char *dest, const char *src, size_t count );
//dest should has enough space
```

<!-- slide -->
# `strcat`

```c{.line-numbers}
char *strcat( char *dest, const char *src );
char *strncat( char *dest, const char *src, size_t count );
//cat is the short of catenate

char str_dest[13]="hello";
char str_src[]="friend";
strcat(str_dest,str_src);
```

<!-- slide -->
# `strcmp`

## :book: P264 字符串不能直接使用关系运算符进行比较

```c{.line-numbers}
int strcmp( const char *lhs, const char *rhs );
int strncmp( const char *lhs, const char *rhs, size_t count );

Negative value if lhs appears before rhs\
    in lexicographical order.

Zero if lhs and rhs compare equal.

Positive value if lhs appears after rhs\
    in lexicographical order.
```

<!-- slide -->
# 10.6 向函数传递字符串:book: P264

## :question: 向函数传递字符串时，既可使用字符数组作函数参数，也可使用字符指针作函数参数

<!-- slide -->
# :book: P265 例10.5

```c{.line-numbers}
//line 15
void  MyStrcpy(char dstStr[], char srcStr[]){
	int  i = 0;
    while (srcStr[i] != '\0'){
    	dstStr[i] = srcStr[i];
    	i++;
   }
	dstStr[i] = '\0';
}
```

<!-- slide -->
# :book: P265 例10.5（续）

```c{.line-numbers}
void  MyStrcpy(char *dstStr, char *srcStr){
	while (*srcStr != '\0'){ 
    	*dstStr = *srcStr;
    	srcStr++;
		dstStr++;
	}
	*dstStr = '\0';
}
```

<!-- slide -->
# 10.7从函数返回字符串指针:book: P268

## :point_right: 字符串函数返回指向 **`dest`** 的指针原因：增加灵活性

```c{.line-numbers}
len = strlen(strcat(str1,"Hello China"));

printf("%s",strcat(str1, str2))
```

## :point_right: 函数间的信息交换通过函数参数和返回值实现

## :point_right: 返回一组值： 传递指针、返回指针、返回结构体

<!-- slide -->
# :book: P265 例10.5（再续）

```c{.line-numbers}
void  MyStrcpy(char *dstStr, char *srcStr){
    int i = 0;
    while (srcStr[i] != '\0') {
        dstStr[i] = srcStr[i];
        ++i;
    }
    dstStr[i] = '\0';
}
```

<!-- slide -->
# 字符处理函数:book: P272

```c{.line-numbersr}
#include <ctype.h>
```

<!-- slide -->
# 数值字符串向数值的转换:book: P277

## 字符串按字符存储，整型按二进制存储

```dot
digraph string_and_int{
    bgcolor=transparent;
    node[shape=record,style=filled,fillcolor=lightblue];

    string_store[label="
        '1'|'2'|'3'|'\0'
    "]

    string_store_ascii[label="
        49|50|51|0
    "]

    string_store_bin[label="
        00110001|00110010|00110011|00000000
    "]

    string_store -> string_store_ascii -> string_store_bin;

   int_store[label="01111011",fillcolor=orange];
}
```

<!-- slide -->
# 数值字符串向数值的转换:book: P277（续）

```c{.line-numbers}
#include<stdlib.h>
double atof( const char* str );

Function discards any whitespace characters\
    (as determined by isspace())\
    until first non-whitespace character is found.

double value corresponding to the contents of str on success.\
    If the converted value falls out of range of\
    the return type, the return value is undefined.\
    If no conversion can be performed, 0.0 is returned.
```

<!-- slide -->
# `atof`

```c{.line-numbers}
printf("%g\n", atof("  -0.0000000123junk"));//-1.23e-08
printf("%g\n", atof("0.012"));//0.012
printf("%g\n", atof("15e16"));//1.5e+17
printf("%g\n", atof("-0x1afp-2"));//-107.75
printf("%g\n", atof("inF"));//inf
printf("%g\n", atof("Nan"));//nan
// UB: out of range of double
printf("%g\n", atof("1.0e+309"));//inf
printf("%g\n", atof("0.0"));//0
// no conversion can be performed
printf("%g\n", atof("junk"));//0
```

<!-- slide -->
# `NAN` and `INF` 

```c{.line-numbers}
#include <math.h>
#define NAN /*implementation defined*/(since C99)
#define INFINITY /*implementation defined*/(since C99)

NAN is the short of "No A Number"
```

<!-- slide -->
# 10.9 知识点小结:book: P278

<!-- slide -->
# 10.10 常见错误小结:book: P279

## `"a"` ==> 语义不明，:point_right:  语义不明时，用注释明确

## `scanf("%s",&a);` ==> `&a` is type of `(*p)[]`

<!-- slide -->
# Thank You && To Be Continue...

## End of This Chapter