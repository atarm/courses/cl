---
presentation:
    minScale: 0.1
    maxScale: 1.5
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
---

<!-- slide -->
# 第11章：指针和数组:book: P285

<!-- slide -->
# 本章学习内容

1. 指针与一维数组间的关系，指针与二维数组间的关系
1. 向函数传递一维数组和二维数组
1. 指针数组，命令行参数
1. 动态数组，动态内存分配

<!-- slide -->
# 指针和一维数组间的关系:book: P285

<!-- slide -->
# 数组的地址

```c{.line-numbers}
int a[10];
/*:warning: Incompatible pointer types initializing 
'int *' with an expression of type 'int (*)[10]'*/
int *pa = &a;
pa = a;//OK
```

## 数值相等，但数据类型不同（位相同，上下文不同）

1. **`a` is `int *` ：** 数组首元素的地址
1. **`&a[0]` is `int *`：** 数组元素的地址
1. **`&a` is `int (*)[]`：** 数组指针地址，代表整个数组

<!-- slide -->
# 数组名和指针变量的区别

1. 数组名是一个编译时确定的常量、类型存储在编译器的符号表中，数组名不能作为左值
1. 指针变量是一个变量，指针变量存储一个内存地址，指针变量可作为左值

## :point_right: 数组名是一个指针常量

<!-- slide -->
# 数组名

>只有在两种场合下，数组名并不用指针常量来表示：就是当数组名作为 **`sizeof`** 或 **`&`** 的操作数时。
>
>**`sizeof`** 返回整个数组的长度，而不是指向数组的指针的长度。
>
>取一个数组名的地址所产生的是一个指向数组的指针，而不是一个指向某个指针常量值的指针。
>
>《C和指针》. Chapter 8.1.1

<!-- slide -->
# 数组下标法与指针运算法:book: P286 - P287

## `p[i]` <==> `*(p+i)`

>:warning:  :book: P287 仅当运算结果仍指向同一数组中的元素时，指针的算术运算才有意义
>:warning:  :book: P287 p++在数值上加了`sizeof(base_type)`个字节

<!-- slide -->
# 11.2 指针和二维数组间的关系:book: P292

1. 可将二维数组看做一个数组，这个数组的元素是一维数组
1. 按行顺序存放元素

<!-- slide -->
# 二维数组名

## :exclamation:  数组名的数值是首元素地址，类型是首元素类型

```c{.line-numbers}
int aa[3][4];

int (*p)[3][4] = &aa;
aa == &aa[0]; //int (*)[4];
*aa == *(aa + 0) == &aa[0][0];//int *
**aa == *(*(aa + 0) + 0) == aa[0][0];//int
```

<!-- slide -->
# 二维数组名（续）

```c{.line-numbers}
int aa[3][4]={{-10,-11,-12,-13},{10,11,12,13},{20,21,22,23}};
int (*paa)[3][4] = &aa;
printf("%d\n",**aa);//-10
printf("%d,%d",(*paa)[2][2], *(*(*paa+1)+1));//22,11
```

<!-- slide -->
# 数组指针 and 指针数组

```c{.line-numbers}
int (*pa)[];//数组指针（pointer point to array）
int *pa[];//指针数组（pointer array）
```

<!-- slide -->
# 行指针 and 列指针

## :point_right: 行指针是数组指针（指向数组的指针）

## :point_right: 列指针是元素指针（指向基础元素的指针）

<!-- slide -->
# 指针的基类型、数组的元素类型

## :point_right: 指针可以指向任一种数据类型

## :point_right: 数组可以包含任一种数据类型

<!-- slide -->
# 11.3 指针数组及其应用:book: P299

## 若干基类型相同的指针构成的数组，称为指针数组

## :point_right: :book: P300 指针数组的主要用途之一是对多个字符串进行操作

## :book: P304 物理排序、索引排序

<!-- slide -->
# 物理排序 ==> :book: P263 例10.4

```c{.line-numbers}
//line 35
if(strcmp(str[j],str[i]) < 0){
    strcpy(temp,str[i]);
    strcpy(str[i],str[j]);
    strcpy(str[j],temp);
}
```

<!-- slide -->
# 索引排序:book: P302 图11-7 :book: P301 例11.4

```c{.line-numbers}
//line 37
if(strcmp(ptr[j],ptr[i]) < 0){
    temp = ptr[i];
    ptr[i] = ptr[j];
    ptr[j] = temp;
}
```

<!-- slide -->
# 命令行参数 :book: P305

```c{.line-numbers}
/*
argc: 参数数量+1
argv[0]: 指向命令本身的字符串的指针
argv[1...argc-1]：指向参数字符串的指针
*/
int main(int argc,char * argv[]);
int main(int argc,char ** argv);
```

<!-- slide -->
# :book: P305 例11.5 :book: P306 图11-9

```c{.line-numbers}
int main(int argc, char *argv[]){
    int  i;
    printf("The number of command line arguments is:%d\n",argc);
	printf("The program name is:%s\n", argv[0]);
	if (argc > 1){
		printf("The other arguments are following:\n");
		for (i = 1; i<argc; i++){
    	    printf("%s\n", argv[i]);
		}
	}
   return 0;
}
```

<!-- slide -->
# 命令行参数

## 如果参数含有空白字符如何输入:question:

## :point_right: 包含在双引号内 ==> 一般不要使用包含空格的路径

## :warning:  `Program Files` 不是一个`CLI`友好的文件路径

<!-- slide -->
# 11.4 动态数组 :book: P306

<!-- slide -->
# 11.4.1 C程序的内存映像:book: P306

![program_memory_image](./00_Images/program_memory_image.png)

<!-- slide -->
# 11.4.2 动态内存分配函数 :book: 307

```c{.line-numbers}
#include<stdlib.h>

//allocate memory
void* malloc( size_t size );
void* calloc( size_t num, size_t size );

//adjust allocated memory
void *realloc( void *ptr, size_t new_size );

//de-allocate allocated memory
void free( void* ptr );
```

<!-- slide -->
# `void *` :book: P308

>**`void *`** 称为 **通用指针（Generic Pointer）** 或 **无类型指针（Typeless Pointer）**

## :point_right: **`void *`** 需要强转到具体类型才能取值

<!-- slide -->
# `malloc()` and `calloc()`:book: P309

```c{.line-numbers}
pf = (float *)calloc(10,sizeof(float));
pf = (float *)malloc(10*sizeof(float))
```

## :question: 使用`calloc()`更明智，因为`calloc()`将分配的内存初始化为0

## :point_right: 在C语言中，显示初始化内存更应该是程序员的责任

<!-- slide -->
# `free()` and `realloc()`

## :book: `realloc()`的返回值是新分配的内存空间的首地址，与原分配的首地址不一定相同

## `free()`和`realloc()`如何知道要释放多少数量的内存空间:question:

<!-- slide -->
# `free()` and `realloc()`（续01）

```c
 ____ The allocated block ____
/                             \
+--------+--------------------+
| Header | Your data area ... |
+--------+--------------------+
          ^
          |
          +-- The address you are given
```

>:point_right: see stackoverflow ==> [How does free know how much to free?](https://stackoverflow.com/questions/1518711/how-does-free-know-how-much-to-free)

<!-- slide -->
# `free()` and `realloc()`（续02）

>:book: P309 因此不要轻易改变该指针变量的值 **（:warning: 教材在此处解释得不清楚）**

## :point_right: 改变指针变量值后 **`free()`** 和 **`realloc()`** 无法找到已分配的内存空间大小

<!-- slide -->
# `free()` and `realloc()`（续03）

```c{.line-numbers}
int* pi = malloc(100*sizeof(int));
++pi;
free(pi);//will crash here
```

<!-- slide -->
# 11.4.3 长度可变的一维动态数组:book: P309

<!-- slide -->
# :book: P309 例11.6

```c{.line-numbers}
//line 11
p = (int *) malloc(n * sizeof(int)); 
if (p == NULL){
    printf("No enough memory!\n");
    exit(1); 
}
//line 21
free(p);
```

<!-- slide -->
# 11.4.4 长度可变的二维动态数组:book: P311

<!-- slide -->
# :book: P311 例11.7

```c{.line-numbers}
//line 13
p = (int *)calloc(m*n, sizeof(int));  
if (p == NULL){
	printf("No enough memory!\n");
	exit(1);
}
//line 22
free(p);
//line 34
scanf("%d", &p[i*n+j]);
```

## :point_right:  指针多维数组以一维数组（线性结构）方式使用

<!-- slide -->
# C99 可变长数组VLA

```c{.line-numbers}
void demo_vla(void){
    int len=0;
    printf("Input the length:");
    scanf("%d",&len);
    int ia[3];
    int ivla[len];
}
```

## VLA是一个语法糖:question:

<!-- slide -->
# 11.5 扩充内容:book: P313

<!-- slide -->
# 11.5.1 常见的内存错误及其对策:book: P313

1. 内存分配不成功就使用:point_right: **`if(p==NULL)`**
1. 内存未初始化就使用:point_right: **`memset()`**
1. 越界访问:point_right: **明确边界**
1. 内存泄露:point_right: **成对编程，谁申请谁释放**
1. 野指针访问:point_right: **see next slice**

<!-- slide -->
# 野指针访问:book: P320

1. 不返回指向局部变量的指针
1. 定义指针变量的同时初始化
1. 函数入口 **`malloc()`**，函数出口 **`free()`**

:point_right: 现代OS，非法内存访问会导致程序异常退出，但大多不至于死机:book: P320

<!-- slide -->
# 11.5.2 缓冲区溢出攻击:book: P321

:point_right: **明确长度**

:point_right: **参数检查**

<!-- slide -->
# Thank You && To Be Continue...

## End of This Chapter