---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Course Overview_"
footer: "@aRoming"
math: katex
---


<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Course Overview

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Table of Contents

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=2 orderedList=false} -->

<!-- code_chunk_output -->

- [Table of Contents](#table-of-contents)
- [High Level](#high-level)
- [C语言](#c语言)

<!-- /code_chunk_output -->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## High Level

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 抽象中的程序

---

<!--
_backgroundColor: white
-->

![height:600](./assets/diagram/program.png)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编程语言

---

#### 编程的主要内容

---

<!--
_backgroundColor: White
_color:
-->

![width:1120](./assets/diagram/lang.png)

---

#### 编程语言的环境

---

<!--
_backgroundColor: White
_color:
-->

![height:600](./assets/diagram/env.png)


---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## C语言

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### C的基础结构

---

<!--
_backgroundColor: White
_color:
-->

![height:600](./assets/diagram/c_basic.png)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### C的控制结构

---

<!--
_backgroundColor: White
_color:
-->

![width:1120](./assets/diagram/c_control_flow.png)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### C的模块化结构

---

<!--
_backgroundColor: White
_color:
-->

![height:600](./assets/diagram/c_modular.png)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### C的内存管理

---

<!--
_backgroundColor: White
_color:
-->

![height:600](./assets/diagram/c_memory.png)

---

1. **编译器控制：** **编译期由编译器确定、加载时运行时系统分配、程序退出时运行时系统释放的内存空间** ，代码段、静态存储区、只读存储区由编译器控制
1. **运行时控制**
    1. **系统控制：** **运行时由运行时系统确定、运行时系统分配、运行时系统释放的内存空间**, 由运行时系统 **自动** 分配和回收内存空间
    1. **程序控制：** **运行时由程序确定、程序申请分配、程序申请释放的内存空间**，程序员编写的程序在运行时动态申请分配、动态申请回收内存空间

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### C的输入输出

---

<!--
_backgroundColor: White
_color:
-->

![height:600](./assets/diagram/c_io.png)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### C的抽象数据类型

---

<!--
_backgroundColor: White
_color:
-->

![height:300](./assets/diagram/c_adt.png)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 基本数据结构与算法

---

<!--
_backgroundColor: White
_color:
-->

![width:1120](./assets/diagram/ds_algo.png)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

**:ok: End of This Slide :ok:**
