# Source Code in K&R

## Structure

1. 以章为目录，每章一个目录，目录下`example`存储教材源码、`exercise`存放练习源码
1. 使用`CMake`，可在`IDE`中导入`CMake Project`

:exclamation: :exclamation: :exclamation:

1. 代码基于`C90`标准，而不是教材中的`C89`标准，尽管差别很小

:exclamation: :exclamation: :exclamation:
